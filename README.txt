TestIterator.java

 // TODO also try with a LinkedList - does it make any difference?
 making the list a LinkedList does not make a big difference - you are still able to run the program correctly
 // TODO what happens if you use list.remove(Integer.valueOf(77))?
  java.util.ConcurrentModificationException error
  Does the same thing - removes all items with value of 77 in the list

TestList.java

 // TODO also try with a LinkedList - does it make any difference?
 making the list a LinkedList does not make a big difference - you are still able to run the program c
 // TODO answer: what does this method do?
 removes item with index 5 - moves the last 77 in the list
 // TODO answer: what does this one do?
 removes item with value of 5 - removes the 5 in the list

TestPerformance.java

    All tests were executed 5 times for each SIZE (10, 100, 1000 and 10000) and
  	the running time is in milliseconds. The running time was recorded using
  	TestPerformance configuration which displays the run time for each method

  	SIZE 10

  	      testArrayListAccess:     27;  29;  42;  29;  36
  	      testArrayListAddRemove:  64;  71;  50;  43;  59
          testLinkedListAccess:    42;  41;  49;  30;  42
          testLinkedListAddRemove: 74;  88;  70;  65; 100


  	SIZE 100

  	      testArrayListAccess:     35;  32;  42;  26;  29
          testArrayListAddRemove:  63;  74;  73;  50;  75
          testLinkedListAccess:    51;  74;  46;  43;  42
          testLinkedListAddRemove: 64;  61;  54;  52;  51


  	SIZE 1000

          testArrayListAccess:      43;  40;  24;  32;  27
          testArrayListAddRemove:  289; 239; 230; 271; 257
          testLinkedListAccess:    512; 470; 425; 452; 440
          testLinkedListAddRemove:  81;  65;  47;  58;  47

  	SIZE 10000

          testArrayListAccess:      33;   33;   27;     84;    35
          testArrayListAddRemove: 2112; 2321; 2897;   3629;  2501
          testLinkedListAccess:   7421; 6402; 7052;  10142;  7384
          testLinkedListAddRemove:  50;   55;   49;    126;    60


 // TODO answer: which of the two lists performs better as the size increases?
  As the size increases, ArrayList does better with Access since there is quicker search with an index versus a LinkedList.
  However, as the size increases, LinkedList does better with Add and Remove since restructuring of the list is much easier than with an ArrayList.